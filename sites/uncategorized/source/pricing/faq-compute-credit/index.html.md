---
layout: markdown_page
title: Managing compute credits FAQ
description: "On this page you can view frequently asked questions for managing compute credits limits"
canonical_path: "/pricing/faq-compute-credit/"
---

# Customer FAQ - Managing compute credits Usage

{:.no_toc}

### On this page

{:.no_toc}

{:toc}

- TOC

## Managing your compute credits usage

**Note:** `CI/CD minutes` is being renamed to `compute credits`. During this transition, you might see references in the UI and documentation to `CI/CD minutes`, `CI minutes`, `pipeline minutes`, `CI pipeline minutes`, `pipeline minutes quota`, and `compute credits`. For more information, see [issue 5218](https://gitlab.com/gitlab-com/Product/-/issues/5218).
{: .note}

**Q. How can I view and manage my compute credits usage?**

A. A Group Owner can view compute credits usage on the Usage page in your Group settings page. 

**Q. How can I view and manage my compute credits usage on public projects?**

A. The compute credits limit is applicable for public projects on GitLab.com as well. 

**Q. How can I reduce the amount of compute credits consumed?**

A. There are a few methods to consider to reduce the number of compute credits consumed:

- Utilize [interruptible](https://docs.gitlab.com/ee/ci/yaml/#interruptible) to abort out of date pipelines;
- Be more selective about when jobs run, for example setting certain jobs to only: run when certain files are changed using [only:changes](https://docs.gitlab.com/ee/ci/yaml/#onlychanges--exceptchanges); and
- Optimize your CI jobs to complete more quickly
- Bring your own runners

Watch this deep dive video on how you can manage your compute credits usage.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GrO-8KtIpRA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Q. What happens if I hit the compute credits allotted limit and forget to purchase additional compute credits?**

A. You will not be able to run new jobs and running jobs may be terminated until you purchase additional compute credits, or until the next month when you receive your monthly allotted compute credits.

**Q. Do compute credits purchased during a trial roll into the paid subscription?**

A. Purchased compute credits will remain on a namespace after the trial completes and will be accessible until they run out.

**Q. Will I be notified before I hit my limit on compute credits?**

A. You will receive notification banners in-app when your group has less than 30%, 5% or exceeded your total allotted compute credits.

<table>
 <tr>
 <td>Percentage Remaining
 </td>
 <td>Users Notified
 </td>
 <td>Notification Type
 </td>
 <td>Frequency
 </td>
 </tr>
 <tr>
 <td>30%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
 <tr>
 <td>5%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
 <tr>
 <td>0%
 </td>
 <td>Owners
 </td>
 <td>Banner
 </td>
 <td>Permanent
 </td>
 </tr>
</table>

**Q. Can I proactively monitor my compute credits usage?**

A. Yes, you can use the [REST API](https://docs.gitlab.com/ee/api/) on GitLab.com to monitor your compute credits usage and integrate this into your standard monitoring tools. Here are a few examples for check plugins and Prometheus integrations:

- https://gitlab.com/6uellerBpanda/check_gitlab/-/tree/master#ci-runner-jobs-duration
- https://github.com/mvisonneau/gitlab-ci-pipelines-exporter

**Q. Are compute credits used on users/customers' own runners accounted into the quota?**

A. No. The compute credits limit is for jobs using GitLab shared runners. Users/Customers can bring their own runners. compute credits used on their own runners are not accounted into the limit.

## Purchasing additional compute credits

**Q. How much does it cost to buy additional compute credits?**

A. compute credits per top-level group (or personal namespace) are $10 per 1,000 minutes and it is valid for one year from the date of purchase. compute credits purchased do not auto-renew. Purchased minutes are only valid for 12 months from the date of purchase or until all minutes are consumed, whichever comes first.

**Q. Will I have different compute credit pricing for Windows and Linux?**

A. No. The pricing is the same regardless of the operating systems.

**Q. How do I purchase additional compute credits?**

A. See [Purchase additional compute credits](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#purchase-additional-cicd-minutes).

## More information

To upgrade to a paid GitLab.com Tier, [purchase online](https://docs.gitlab.com/ee/subscriptions/gitlab_com/) or contact [GitLab Sales](https://page.gitlab.com/ci-minutes.html)

To address your questions and feedback, we have created a space in the [GitLab Community Forum](https://forum.gitlab.com/t/ci-cd-minutes-for-free-tier/40241), which is actively monitored by GitLab Team members and Product Managers involved with this change.
